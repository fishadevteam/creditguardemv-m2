<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ImportData implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $payment = $observer->getEvent()->getData('payment');
        $data = $observer->getEvent()->getData('input');

        if(
            $payment instanceof  \Magento\Quote\Api\Data\PaymentInterface
            && $data instanceof \Magento\Framework\DataObject
        ){
            $data->setQuoteId($payment->getQuote()->getId());
        }

        return $this;
    }
}
