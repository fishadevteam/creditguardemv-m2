define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'creditguard_emv',
                component: 'Fisha_CreditGuardEMV/js/view/payment/method-renderer/creditguard-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);