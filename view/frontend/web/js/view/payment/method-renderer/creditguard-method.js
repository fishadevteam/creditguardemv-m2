define([
    'ko',
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/error-processor',
], function (ko, $, Component, selectPaymentMethodAction, checkoutData, quote, fullScreenLoader, errorProcessor) {
    'use strict';

    var checkoutConfig = window.checkoutConfig,
        agreementsConfig = checkoutConfig ? checkoutConfig.checkoutAgreements : {};

    return Component.extend({
        defaults: {
            template: 'Fisha_CreditGuardEMV/payment/creditguard',
            createTransaction: '',
            okMessage: '',
            orderIsPlaced: false,
            iframeHeight: 600,
            fail: false
        },

        /** Returns send check to info */
        getMailingAddress: function () {
            return window.checkoutConfig.payment.checkmo.mailingAddress;
        },

        isChecked: ko.computed(function () {
            if(quote.paymentMethod() && quote.paymentMethod().method !== 'creditguard_emv'){
                $('#cg_ifm').height(0);
            } else {
                if ($('.checkout-agreements-block.creditguard_emv input').is(":checked")) {
                    $('#cg_ifm').height(600);
                }
            }

            return quote.paymentMethod() ? quote.paymentMethod().method : null;
        }),

        /**
         * @param forceLoad
         * @returns {boolean}
         */
        selectPaymentMethod: function (forceLoad) {
            // selectPaymentMethodAction(this.getData());
            var paymentMethod = this.getData()
            if (paymentMethod) {
                paymentMethod.__disableTmpl = {
                    title: true
                };
            }
            quote.paymentMethod(paymentMethod);

            checkoutData.setSelectedPaymentMethod(this.item.method);

            if (agreementsConfig.isEnabled) {
                $(document).on('change', '.checkout-agreements-block.creditguard_emv input', function () {
                    this.createTransaction(null);
                    $('#cg_ifm').height(0);
                    if ($('.checkout-agreements-block.creditguard_emv input').is(":checked")) {
                        this.getIframe();
                    }
                }.bind(this));
            } else {
                this.getIframe();
            }

            /**
             * forceLoad has value of 'true' when triggered from KO template
             * app/design/frontend/Fisha/papaya/Magento_Checkout/web/template/payment-methods/list.html
             * we need to load creditguard iframe right after payment method is rendered on the page
             */
            if (forceLoad === true) {
                this.getIframe();
            }

            return true;
        },

        getIframe: function () {
            $.ajax({
                showLoader: true,
                url: '/creditguard_emv/transaction/token',
                data: {'email': quote.guestEmail},
                type: "POST",
                dataType: 'text'
            }).fail(function (data) {
                window.location.reload();
            }).done(function (data) {
                this.createTransaction(data);
                //this.isPlaceOrderActionAllowed(false);
                $('#cg_ifm').height(this.iframeHeight);

            }.bind(this));
        },

        initialize: function () {
            this._super()
                .observe([
                    'createTransaction',
                    'orderIsPlaced',
                    'fail'
                ]);

            this.selectPaymentMethod();
        },

        /**
         * Get payment method data
         */
        getData: function () {
            var message = '';
            if(typeof(this.okMessage) == 'object' && this.okMessage.hasOwnProperty('cardToken')){
                message = this.okMessage;
            }
            return {
                'method': this.item.method,
                'additional_data': message
            };
        },

        validateGwData: function(gwData) {
            var result = false;

            if(
                typeof(gwData) == 'object'
                && gwData.hasOwnProperty('cardToken')
                && gwData.cardToken.length > 1
            ){
                result = true;
            }

            return result;
        },

        /**
         * @return {*}
         */
        getPlaceOrderDeferredObject: function () {
            return this._super()
                .fail(function () {
                    fullScreenLoader.stopLoader();
                    this.reloadIframe();
                    this.fail(true);
                }.bind(this)).always(function () {
                    this.reloadIframe();
                }.bind(this));
        },

        reload: function() {
            if (this.fail()) {
                this.orderIsPlaced(false);
                this.setLocalStorageProcessData(false);
                this.getIframe();
                this.fail(false);
            }
        },

        bindCGEvent: function() {
            this.clearLocalStorageProcessData();
            fullScreenLoader.stopLoader();

            $(document).on('CGResponse', function (e) {
                var gwData = e.detail,
                    localCheck = this.getLocalStorageProcessData();

                var errorMessage = this.getErrorMessage(gwData);

                if (errorMessage) {
                    var errorObject = {
                        status: 500,
                        responseText: errorMessage
                    };
                    errorProcessor.process(errorObject);
                    return false;
                }

                if (
                    quote.paymentMethod().method == 'creditguard_emv'
                    && this.validateGwData(gwData) === true
                    && this.orderIsPlaced() === false
                    && localCheck === false
                ) {
                    this.orderIsPlaced(true);
                    this.setLocalStorageProcessData(this.orderIsPlaced());

                    fullScreenLoader.startLoader();

                    this.okMessage = gwData;
                    this.orderIsPlaced(this.placeOrder());
                }

            }.bind(this));
        },

        getErrorMessage: function(gwData) {
            var result = null;

            if (typeof(gwData) == 'object'
                && gwData.hasOwnProperty('errorMessage')
            ) {
                result = gwData.errorMessage;
            }

            return result;
        },

        /**
         * After place order callback
         */
        afterPlaceOrder: function () {
            $('#cg_ifm').hide();
            this.clearLocalStorageProcessData();
            fullScreenLoader.stopLoader();
        },

        setLocalStorageProcessData: function (flag) {
            localStorage.setItem(this.item.method, flag);
        },

        getLocalStorageProcessData: function () {
            return JSON.parse(localStorage.getItem(this.item.method));
        },

        clearLocalStorageProcessData: function () {
            localStorage.setItem(this.item.method, false);
        },

        reloadIframe: function () {
            document.getElementById('cg_ifm').src += '';
        }

    });

});
