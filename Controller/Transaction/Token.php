<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Controller\Transaction;

use Fisha\CreditGuardEMV\Model\Transaction\TokenFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Token extends Action
{

    /**
     * @var TokenFactory
     */
    protected $tokenFactory;

    /**
     * @param Context $context
     * @param TokenFactory $tokenFactory
     */
    public function __construct(
        Context $context,
        TokenFactory $tokenFactory
    ) {
        parent::__construct($context);
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        return $this->getResponse()
            ->setBody($this->tokenFactory->create()->execute());
    }

}
