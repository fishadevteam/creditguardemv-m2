<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Controller\Transaction;

use Fisha\CreditGuardEMV\Api\PaymentInfoRepositoryInterface;
use Fisha\CreditGuardEMV\Model\Config\Source\TransactionType;
use Fisha\CreditGuardEMV\Model\Logger\Logger;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class Ok extends Action
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var PaymentInfoRepositoryInterface
     */
    private PaymentInfoRepositoryInterface $paymentInfoRepository;

    /**
     * @var Session
     */
    private Session $checkoutSession;

    /**
     * @var string
     */
    private string $errorMessage = '';

    /**
     * Ok constructor.
     * @param Context $context
     * @param Logger $logger
     * @param PaymentInfoRepositoryInterface $paymentInfoRepository
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        Logger $logger,
        PaymentInfoRepositoryInterface $paymentInfoRepository,
        Session $checkoutSession
    ) {
        $this->logger = $logger;
        parent::__construct($context);
        $this->paymentInfoRepository = $paymentInfoRepository;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        $this->logger->info('ok_page-' . json_encode($this->getRequest()->getParams()));
        try {
            $this->updatePaymentInfo();
        } catch (LocalizedException $e) {
            $this->errorMessage = $e->getMessage();
        }
        $this->getResponse()->setBody($this->getCGScript());
    }

    /**
     * @return string
     */
    protected function getCGScript()
    {
        $cgArgs = $_GET;
        if ($this->errorMessage) {
            $cgArgs['errorMessage'] = $this->errorMessage;
        }
        $cgResponse = \Zend_Json::encode($cgArgs);
        return <<<SCRIPT
<script type="text/javascript">
var cgEvent = document.createEvent('CustomEvent');
cgEvent.initCustomEvent('CGResponse', true, true, $cgResponse);
window.top.document.dispatchEvent(cgEvent);
</script>
SCRIPT;
    }

    /**
     * @throws LocalizedException
     */
    private function updatePaymentInfo(): void
    {
        $transactionId = $this->getRequest()->getParam('txId');
        if (!$transactionId) {
            $this->logger->error(
                'There is no txId param in the success CreditGuard response.', $this->getRequest()->getParams()
            );
            throw new LocalizedException(__('CreditGuard Payment processingError: No transaction ID received. Please contact to site administrator'));
        }
        try {
            $paymentInfoModel = $this->paymentInfoRepository->getByTransactionId($transactionId);
            $paymentInfoModel->setCardMask($this->getRequest()->getParam('cardMask'));
            $paymentInfoModel->setType(TransactionType::PAYMENT);
            $this->paymentInfoRepository->save($paymentInfoModel);
            $this->checkoutSession->setQuoteId($paymentInfoModel->getQuoteId());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new LocalizedException(
                __('CreditGuard Payment processing transaction %1 error. Please contact to site administrator', $transactionId)
            );
        }
    }
}
