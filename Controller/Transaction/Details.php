<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Controller\Transaction;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Details extends Action
{

    /**
     * @var \Fisha\CreditGuardEMV\Model\Transaction\Details
     */
    protected $_details;

    /**
     * @param Context $context
     * @param \Fisha\CreditGuardEMV\Model\Transaction\Details $details
     */
    public function __construct(
        Context $context,
        \Fisha\CreditGuardEMV\Model\Transaction\Details $details
    ) {
        $this->_details = $details;
        parent::__construct($context);
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        return $this->getResponse()
            ->setBody($this->_details->getDetails(
                $this->getRequest()->getParam('req_id', ''),
                $this->getRequest()->getParam('tx_id', '')
            ));
    }

}
