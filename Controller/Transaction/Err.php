<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Controller\Transaction;

use Fisha\CreditGuardEMV\Model\Logger\Logger;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Err extends Action
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Context $context
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Logger $logger
    ) {
        $this->logger      = $logger;
        parent::__construct($context);
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        $msg = $this->getRequest()->getParam('msg');

        if (isset($msg) && is_numeric($msg)) {
            $this->logger->error('Error Code: ' . (int)$msg);
            return $this->getResponse()
                ->setBody('Error Code: ' . (int)$msg);
        } elseif (isset($msg) && is_string($msg)) {
            $this->logger->error('Error Message: ' . $msg);
            return $this->getResponse()
                ->setBody('Error Message: ' . $msg);
        }
    }
}
