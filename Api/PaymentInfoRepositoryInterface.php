<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Api;

use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface PaymentInfoRepositoryInterface
{
    /**
     * Save PaymentInfo
     * @param \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface $paymentInfo
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface $paymentInfo
    );

    /**
     * Retrieve PaymentInfo
     * @param string $paymentinfoId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($paymentinfoId);

    /**
     * Retrieve PaymentInfo matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PaymentInfo
     * @param \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface $paymentInfo
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface $paymentInfo
    );

    /**
     * Delete PaymentInfo by ID
     * @param string $paymentinfoId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($paymentinfoId);

    /**
     * @param string $transactionId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     * @throws NoSuchEntityException
     */
    public function getByTransactionId(string $transactionId);

    /**
     * @param string $quoteId
     * @param int $transactionType
     * @return PaymentInfoInterface
     * @throws NoSuchEntityException
     */
    public function getByQuoteId(string $quoteId, int $transactionType): PaymentInfoInterface;
}
