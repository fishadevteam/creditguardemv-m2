<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Api\Data;

interface PaymentInfoSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PaymentInfo list.
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface[]
     */
    public function getItems();

    /**
     * Set quote_id list.
     * @param \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

