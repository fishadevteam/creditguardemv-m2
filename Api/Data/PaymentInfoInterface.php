<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Api\Data;

use Magento\Framework\Exception\NoSuchEntityException;

interface PaymentInfoInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const REQUEST_ID = 'request_id';
    const CARD_MASK = 'card_mask';
    const QUOTE_ID = 'quote_id';
    const ENTITY_ID = 'entity_id';
    const TRANSACTION_ID = 'transaction_id';
    const TYPE = 'type';

    /**
     * Get entity_id
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     * @param string $entityId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setEntityId($entityId);

    /**
     * Get quote_id
     * @return string|null
     */
    public function getQuoteId();

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setQuoteId($quoteId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface $extensionAttributes
    );

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId();

    /**
     * Set request_id
     * @param string $requestId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setRequestId($requestId);

    /**
     * Get transaction_id
     * @return string|null
     */
    public function getTransactionId();

    /**
     * Set transaction_id
     * @param string $transactionId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setTransactionId($transactionId);

    /**
     * Get card_mask
     * @return string|null
     */
    public function getCardMask();

    /**
     * Set card_mask
     * @param string $cardMask
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setCardMask($cardMask);

    /**
     * Get card_mask
     * @return int
     */
    public function getType(): int;

    /**
     * Set card_mask
     * @param int $type
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setType(int $type);
}
