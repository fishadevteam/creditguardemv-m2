# Fisha Credit Guard EMV Extension 
This module is a customized version of default Credit Guard extension (https://bitbucket.org/fishadevteam/m2creditguard) in order to work with EMV terminal.

## Installation
**Please pay attention to the versions.**

^1.0 versions works till 2.4.3

^2.0 versions works from 2.4.5

### Git
Make a git clone in the root folder of the project

```bash
git clone git@bitbucket.org:fishadevteam/creditguardemv-m2.git app/code/Fisha/CreditGuardEMV
```

### Composer
    composer config repositories.fisha-creditguard-emv vcs git@bitbucket.org:fishadevteam/creditguardemv-m2.git
    composer require fisha/creditguard-emv:<version>

### Re-build magento project
```bash
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento setup:static-content:deploy
```
