<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model\ResourceModel;

class PaymentInfo extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('fisha_cg_transactions', 'entity_id');
    }
}
