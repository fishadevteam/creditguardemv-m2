<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Fisha\CreditGuardEMV\Model\PaymentInfo::class,
            \Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo::class
        );
    }
}
