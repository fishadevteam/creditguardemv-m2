<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Payment\Model\MethodInterface;

class PaymentAction implements OptionSourceInterface
{

    /**
     * {@inheritDoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => MethodInterface::ACTION_AUTHORIZE,
                'label' => __('Authorize')
            ],
            [
                'value' => MethodInterface::ACTION_AUTHORIZE_CAPTURE,
                'label' => __('Capture')
            ],
            [
                'value' => \Fisha\CreditGuardEMV\Model\Creditguard::ACTION_VERIFY_ONLY,
                'label' => __('Verify Only')
            ]

        ];
    }

}
