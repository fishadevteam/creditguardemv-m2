<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model\Config\Source;

class TransactionType
{
    const IFRAME_URL = 1;
    const PAYMENT = 2;
}
