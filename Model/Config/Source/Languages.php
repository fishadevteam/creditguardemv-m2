<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Languages implements ArrayInterface
{

    /**
     * {@inheritDoc}
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'HEB', 'label' => __('Hebrew')],
            ['value' => 'ENG', 'label' => __('English')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['HEB' => __('Hebrew'), 'ENG' => __('English')];
    }

}
