<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model;

use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface;
use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PaymentInfo extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var string
     */
    protected $_eventPrefix = 'fisha_creditguardemv_paymentinfo';

    /**
     * @var PaymentInfoInterfaceFactory
     */
    protected $paymentinfoDataFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PaymentInfoInterfaceFactory $paymentinfoDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo $resource
     * @param \Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PaymentInfoInterfaceFactory $paymentinfoDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo $resource,
        \Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo\Collection $resourceCollection,
        array $data = []
    ) {
        $this->paymentinfoDataFactory = $paymentinfoDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve paymentinfo model with paymentinfo data
     * @return PaymentInfoInterface
     */
    public function getDataModel()
    {
        $paymentinfoData = $this->getData();
        $paymentinfoDataObject = $this->paymentinfoDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $paymentinfoDataObject,
            $paymentinfoData,
            PaymentInfoInterface::class
        );

        return $paymentinfoDataObject;
    }
}
