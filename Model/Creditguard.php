<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model;

use Fisha\CreditGuardEMV\Model\Config\Source\TransactionType;
use Fisha\CreditGuardEMV\Model\Transaction\DetailsFactory;
use Fisha\CreditGuardEMV\Model\Transaction\RecreateFactory;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Fisha\CreditGuardEMV\Model\Transaction\RefundFactory;
use Magento\Payment\Model\Method\Logger;

class Creditguard extends AbstractMethod
{
    const ACTION_VERIFY_ONLY = 'verify';

    const TX_ID_REGISTRY_KEY = 'current_emv_tx_id';

    protected $_code = 'creditguard_emv';

    protected $_isGateway = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_minAmount = null;
    protected $_maxAmount = null;

    protected static $cgErrorCodes = [
        '001',
        '002',
        '003',
        '004'
    ];

    protected static $ccErrorCodes = [
        '006',
        '007',
        '017',
        '033',
        '036',
        '039',
        '058',
        '059'
    ];

    /**
     * @var RefundFactory
     */
    protected $refundFactory;

    /**
     * @var DetailsFactory
     */
    protected $detailsFactory;

    /**
     * @var RecreateFactory
     */
    protected $recreateFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Stdlib\ArrayManager
     */
    protected $arrayManager;
    protected PaymentInfoRepository $paymentInfoRepository;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param PaymentInfoRepository $paymentInfoRepository
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param RefundFactory $refundFactory
     * @param DetailsFactory $detailsFactory
     * @param RecreateFactory $recreateFactory
     * @param ManagerInterface $messageManager
     * @param \Magento\Framework\Stdlib\ArrayManager $arrayManager
     * @param Logger $logger
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @param DirectoryHelper|null $directory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        PaymentInfoRepository $paymentInfoRepository,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        RefundFactory $refundFactory,
        DetailsFactory $detailsFactory,
        RecreateFactory $recreateFactory,
        ManagerInterface $messageManager,
        \Magento\Framework\Stdlib\ArrayManager $arrayManager,
        Logger $logger,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );

        $this->refundFactory = $refundFactory;
        $this->detailsFactory = $detailsFactory;
        $this->recreateFactory = $recreateFactory;
        $this->messageManager = $messageManager;
        $this->arrayManager = $arrayManager;
        $this->paymentInfoRepository = $paymentInfoRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function assignData(DataObject $data)
    {
        $txId = $data->getAdditionalData('txId');
        $uniqueId = $data->getAdditionalData('uniqueID');
        $quoteId = $data->getQuoteId();
        $transactionDetails = [];

        if (!$txId && (int)$quoteId<1) {
            return $this;
        }

        if((int)$quoteId > 0 && !$txId){
            $transactionDetails = $this->getTransactionDataByQuoteId($quoteId, TransactionType::IFRAME_URL);

            if(isset($transactionDetails['cg_tx_id'])) $txId = $transactionDetails['cg_tx_id'];

        }

        if(count($transactionDetails)<1 && $txId && $uniqueId){
            $transactionDetails = $this->getTransactionDetails($txId, $uniqueId);
        }

        if(count($transactionDetails)<1){
           return $this;
        }
        
        $this->getInfoInstance()
            ->setAdditionalInformation($transactionDetails);

        $this->_registry->register(self::TX_ID_REGISTRY_KEY, $txId);

        return parent::assignData($data);
    }

    /**
     * @param $txId
     *
     * @return bool
     */
    public function validateTxId($txId)
    {
        $result = true;
        if (
            (bool) $txId === true &&
            $txId === $this->_registry->registry(self::TX_ID_REGISTRY_KEY)
        ) {
            $result = false;
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function authorize(InfoInterface $payment, $amount)
    {
        if (!$this->canAuthorize()) {
            throw new LocalizedException(__('The authorize action is not available.'));
        }

        $this->paymentRequest($payment, $amount, 'authorize');

        if($this->getConfigPaymentAction() == self::ACTION_VERIFY_ONLY){

            $this->addSuccessMessage(__('CG action is Verify Only. Other actions disabled.'));

        }

        $this->addSuccessMessage(__('CreditGuard EMV payment was placed'));

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function capture(InfoInterface $payment, $amount)
    {
        if (!$this->canCapture()) {
            throw new LocalizedException(__('The capture action is not available.'));
        }

        if($this->getConfigPaymentAction() == self::ACTION_VERIFY_ONLY){

            $this->addSuccessMessage(__('CG action is Verify Only. Other actions disabled.'));

            return $this;

        }

        $result = $this->paymentRequest($payment, $amount, 'capture');

        $this->addSuccessMessage(__('CreditGuard EMV Payment Was Captured Successfully'));
        $tranId = $this->arrayManager->get(
            $this->arrayManager->findPath('tranId', $result),
            $result
        );
        if ($tranId) {
            $payment->setTransactionId($tranId);
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function refund(InfoInterface $payment, $amount)
    {
        if (!$this->canRefund()) {
            throw new LocalizedException(__('The refund action is not available.'));
        }

        if($this->getConfigPaymentAction() == self::ACTION_VERIFY_ONLY){

            $this->addSuccessMessage(__('CG action is Verify Only. Other actions disabled.'));

            return $this;

        }

        $result = $this->refundFactory->create()
            ->refund($payment, $amount);

        $this->validatePaymentResponse($result);

        $this->addSuccessMessage(__('CreditGuard EMV Payment Was Refunded Successfully'));

        return $this;
    }

    /**
     * @param $errorCode
     * @return Phrase
     */
    private function getErroMsg($errorCode)
    {
        $msg = __('System error Please contact a customer support or customer service representative');
        if (\in_array($errorCode, static::$cgErrorCodes, false)) {
            $msg = __('The deal was not honored by the credit card company.');
        } elseif (\in_array($errorCode, static::$ccErrorCodes, false)) {
            $msg = __('The credit card details is not valid and has not been checked by the credit card company.');
        }

        return $msg;
    }

    /**
     * @param string|Phrase $message
     */
    private function addSuccessMessage($message)
    {
        if ($this->_scopeConfig->isSetFlag(Config::XML_PATH_SHOW_SUCCESS_MESSAGE)) {
            if ($message instanceof Phrase) {
                $message = $message->render();
            }
            $this->messageManager->addSuccessMessage($message);
        }
    }

    /**
     * @param $payment
     * @param float $amount
     * @return array|string
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function paymentRequest($payment, float $amount, $method)
    {

        $authMode = $method === 'capture' && $this->getConfigPaymentAction() === AbstractMethod::ACTION_AUTHORIZE;

        if (!$payment->getAdditionalInformation('cg_card_id')) {
            $this->assignCGData($payment);
        }

        if($this->getConfigPaymentAction() == self::ACTION_VERIFY_ONLY){

            $this->addSuccessMessage(__('CG action is Verify Only. Other actions disabled.'));

            return $payment->getAdditionalInformation();

        }

        $result = $this->recreateFactory->create()
            ->doRecreate(
                $payment->getOrder()->getId(),
                $amount,
                $payment->getAdditionalInformation('cg_card_exp'),
                $payment->getAdditionalInformation('cg_card_id'),
                $payment->getAdditionalInformation('cg_num_payments'),
                $payment->getAdditionalInformation('cg_first_payment'),
                $payment->getAdditionalInformation('cg_periodical_payment'),
                $payment->getAdditionalInformation('cg_card_bin'),
                $authMode ? $payment->getAdditionalInformation('cg_auth_number') : '',
                'autoComm',
                $authMode ? '' : $payment->getAdditionalInformation('cg_session_cd')
            );

        $this->logger->debug(
            [$result],
            null,
            true
        );

        $this->validatePaymentResponse($result);

        $deal = $this->arrayManager->get(
            $this->arrayManager->findPath('doDeal', $result),
            $result
        );

        $payment->setAdditionalInformation(
            array_merge($payment->getAdditionalInformation(), [
                'cc_type' => ($deal['cardBrand'] ?? '') === 'PrivateLabel' ?
                    $deal['creditCompany'] ?? '' :
                    $deal['cardBrand'] ?? '',
                'cg_status_text' => $deal['statusText'] ?? '',
                'cg_card_mask' => $deal['cardMask'] ?? '',
                'cg_card_token' => $deal['cardId'] ?? '',
                'cg_auth_number' => $deal['authNumber'] ?? ''
            ])
        );

        if (isset($deal['invoice'])) {
            $this->addSuccessMessage(__('CreditGuard EMV Invoice Created.'));
        }

        return $result;
    }

    /**
     * @param $result
     * @throws LocalizedException
     */
    protected function validatePaymentResponse($result)
    {
        if (!\is_array($result)) {
            $error = \is_string($result) ?
                __($result) :
                __('Something went wrong during capture CG request.');
            throw new LocalizedException($error);
        }

        $status = $this->arrayManager->get(
            $this->arrayManager->findPath('result', $result),
            $result
        );

        if ($status !== '000') {
            throw new LocalizedException(
                $this->getErroMsg($status)
            );
        }
    }

    private function getTransactionDetails($txId, $uniqueId)
    {
        if ($this->validateTxId($txId) === false) {
            $this->logger->debug(
                ['ERROR' => 'Order with ' . $txId . ' processed already '],
                null,
                true
            );

            throw new LocalizedException(
                __('Order with the same payment ID processed')
            );
        }

        $details = $this->detailsFactory->create()
            ->getDetails(null, $txId);

        $this->validatePaymentResponse($details);

        $deal = $this->arrayManager->get(
            $this->arrayManager->findPath('doDeal', $details),
            $details
        );

        return [
            'cg_personal_id' => $deal['id'] ?? '',
            'cg_unique_id' => $uniqueId,
            'cg_tx_id' => $txId,
            'cg_total' => $deal['total'] ?? '',
            'cg_card_exp' => $deal['cardExpiration'] ?? '',
            'cg_card_id' => $deal['cardId'] ?? '',
            'cg_num_payments' => $deal['numberOfPayments'] ?? '',
            'cg_first_payment' => $deal['firstPayment'] ?? '',
            'cg_periodical_payment' => $deal['periodicalPayment'] ?? '',
            'cg_card_bin' => $deal['cardBin'] ?? '',
            'cg_auth_number' => $deal['authNumber'] ?? '',
            'cg_session_cd' => $deal['sessionCD'] ?? '',
        ];
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function assignCGData(\Magento\Sales\Model\Order\Payment $payment)
    {
        $quoteId = $payment->getOrder()->getQuoteId();

        $transactionData = $this->getTransactionDataByQuoteId($quoteId, TransactionType::PAYMENT);

        foreach ($transactionData as $key => $value) {
            $payment->setAdditionalInformation($key, $value);
        }
    }

    /**
     * @param $quoteId
     * @param $transactiontype
     * @return array
     */
    private function getTransactionDataByQuoteId($quoteId, $transactiontype) : array
    {
        $transactionData = [];

        try{
            $transaction = $this->paymentInfoRepository->getByQuoteId($quoteId, $transactiontype);

            $transactionData = $this->getTransactionDetails(
                $transaction->getTransactionId(),
                $transaction->getRequestId(),
            );

        } catch (\Exception $e){

        }

        return $transactionData;
    }
}
