<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    const XML_PATH_CREDITGUARD_TID = 'payment/creditguard_emv/tid';
    const XML_PATH_CREDITGUARD_MID = 'payment/creditguard_emv/mid';
    const XML_PATH_CREDITGUARD_USER = 'payment/creditguard_emv/user';
    const XML_PATH_CREDITGUARD_PASS = 'payment/creditguard_emv/password';
    const XML_PATH_CREDITGUARD_URL = 'payment/creditguard_emv/cg_gateway_url';
    const XML_PATH_CREDITGUARD_INVOICE = 'payment/creditguard_emv/invoice';
    const XML_PATH_CREDITGUARD_INFO = 'payment/creditguard_emv/company_info';
    const XML_PATH_CREDITGUARD_COMMENT = 'payment/creditguard_emv/invoice_comments';
    const XML_PATH_CREDITGUARD_SUBJECT = 'payment/creditguard_emv/subject';
    const XML_PATH_CREDITGUARD_PAYMENTS = 'payment/creditguard_emv/payments';
    const XML_PATH_CREDITGUARD_LNG = 'payment/creditguard_emv/language';
    const XML_PATH_CREDITGUARD_TOTAL = 'payment/creditguard_emv/use_grand_total';
    const XML_PATH_CREDITGUARD_USE_ID = 'payment/creditguard_emv/use_id';
    const XML_PATH_CREDITGUARD_PAYMENT_ACTION = 'payment/creditguard_emv/payment_action';
    const XML_PATH_SHOW_SUCCESS_MESSAGE = 'payment/creditguard_emv/show_success_msg';
    const XML_PATH_PAYMENT_PAGE_DATA = 'payment/creditguard_emv/payment_page_data';
    const XML_PATH_USE_PPS_JSON_CONFIG = 'payment/creditguard_emv/use_pps_json_config';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SerializerInterface $serializer
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_TID);
    }

    /**
     * @return mixed
     */
    public function getMid()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_MID);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_USER);
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_PASS);
    }

    /**
     * @return mixed
     */
    public function getCGUrl()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_URL);
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_LNG);
    }

    /**
     * @return mixed
     */
    public function getPayments()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_PAYMENTS);
    }

    /**
     * @return mixed
     */
    public function getInvoice()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_INVOICE);
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_INFO);
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_COMMENT);
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_SUBJECT);
    }

    /**
     * @return mixed
     */
    public function useTotal()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_TOTAL);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function configVal(string $key = '')
    {
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function useID()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_USE_ID);
    }

    /**
     * @return mixed
     */
    public function usePpsJsonConfig()
    {
        return $this->configVal(self::XML_PATH_USE_PPS_JSON_CONFIG);
    }

    public function getPaymentPageData()
    {
        return $this->configVal(self::XML_PATH_PAYMENT_PAGE_DATA);
    }

    public function getPaymentAction()
    {
        return $this->configVal(self::XML_PATH_CREDITGUARD_PAYMENT_ACTION);
    }

    /**
     * @param $grandTotal
     *
     * @return int
     */
    public function getPaymentsNum($grandTotal)
    {
        $payments = 1;
        $grandTotal = floatval($grandTotal);
        $config = $this->configVal(self::XML_PATH_CREDITGUARD_PAYMENTS);
        if ($config) {
            $ranges = $this->serializer->unserialize($config);
            if (is_array($ranges)) {
                foreach ($ranges as $range) {
                    if (($grandTotal > (float)$range['from']) && ($grandTotal <= (float)$range['to'])) {
                        $payments = $range['payments'];
                        break;
                    }
                }
            }
        }

        return (int) $payments;
    }

}
