<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Transaction;

use Fisha\CreditGuardEMV\Model\Config;
use Fisha\CreditGuardEMV\Model\Logger\Logger;
use Magento\Framework\Convert\Xml;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\Stdlib\ArrayManager;
use SimpleXMLElement;

class AbstractTransaction
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ClientFactory
     */
    protected $clientFactory;

    /**
     * @var Xml
     */
    protected $xmlConverter;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(
        Config $config,
        ClientFactory $clientFactory,
        Xml $xmlConverter,
        ArrayManager $arrayManager,
        Logger $logger
    ) {
        $this->config = $config;
        $this->clientFactory = $clientFactory;
        $this->xmlConverter = $xmlConverter;
        $this->arrayManager = $arrayManager;
        $this->logger = $logger;
    }

    /**
     * @param array $xml
     * @return string
     */
    protected function preparePostString($xml = [])
    {
        $post = 'user=' . $this->config->getUser();
        $post .= '&password=' . $this->config->getPassword();
        $t = $this->arrayToXml(
            $xml,
            new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><ashrait/>')
        )->asXML();
        $t = str_replace('<?xml version="1.0" encoding="utf-8"?>\n', '', $t);
        $post .= '&int_in=' . $t;

        $this->logger->info('payment_request-' .
            preg_replace('#(' . 'password=' . ')(.*)(' . '&' . ')#si', '$1******$3', $post));

        return $post;
    }

    /**
     * @param array $arr
     * @param SimpleXMLElement $xml
     * @return SimpleXMLElement
     */
    public function arrayToXml(array $arr, SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            \is_array($v)
                ? $this->arrayToXml($v, $xml->addChild($k))
                : $xml->addChild($k, $v);
        }

        return $xml;
    }

    /**
     * @param string $xml
     * @return array
     */
    public function xmlStringToArray(string $xmlString)
    {
        $result = [];
        $clearArray = static function (&$array) use (&$clearArray) {
            if (\is_array($array)) {
                foreach ($array as &$value) {
                    if (\is_array($value)) {
                        if (empty($value)) {
                            $value = '';
                        } else {
                            $clearArray($value);
                        }
                    }
                }
            }
        };

        if ($xml = simplexml_load_string($xmlString)) {
            $result = $this->xmlConverter->xmlToAssoc($xml);
            $clearArray($result);
        }

        return $result;
    }

    /**
     * @param array $post
     * @return array
     */
    protected function request($post)
    {
        $result = false;
        $error = null;
        $post = $this->preparePostString($post);
        $client = $this->clientFactory->create();

        try {
            $client->post($this->config->getCGUrl(), $post);

            if ($result = $client->getBody()) {
                if (stripos($result, 'iso-8859-8')) {
                    $result = iconv('utf-8', 'iso-8859-8', $result);
                }
                $result = $this->xmlStringToArray($result);
            }

        } catch (\Exception $e) {
            $error = $e;
        }

        return [$result, $error];
    }

    /**
     * @param $params
     */
    public function addPpsJsonConfig(&$params): void
    {
        if (!$this->config->usePpsJsonConfig()) {
            return;
        }

        $ppsJSONConfig = preg_replace('/\s+/', ' ', $this->config->getPaymentPageData());
        $params['request']['doDeal']['paymentPageData']['ppsJSONConfig'] = $ppsJSONConfig;
    }
}
