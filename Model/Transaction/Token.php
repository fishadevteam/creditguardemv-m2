<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Transaction;

use Exception;
use Fisha\CreditGuardEMV\Model\Config;
use Fisha\CreditGuardEMV\Model\Config\Source\TransactionType;
use Fisha\CreditGuardEMV\Model\Logger\Logger;
use Fisha\CreditGuardEMV\Model\PaymentInfoFactory;
use Fisha\CreditGuardEMV\Model\PaymentInfoRepository;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Framework\Convert\Xml;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;

/**
 * Class Token
 * @package Fisha\CreditGuardEMV\Model\Transaction
 */
class Token extends AbstractTransaction
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var PaymentInfoFactory
     */
    private $paymentInfoFactory;

    /**
     * @var string[]
     */
    private $validateFields = [
        'uniqueid',
        'token',
    ];

    /**
     * @var PaymentInfoRepository
     */
    private PaymentInfoRepository $paymentInfoRepository;

    /**
     * @param Config $config
     * @param ClientFactory $clientFactory
     * @param Xml $xmlConverter
     * @param ArrayManager $arrayManager
     * @param UrlInterface $urlBuilder
     * @param Session $customerSession
     * @param Cart $cart
     * @param Logger $logger
     * @param PaymentInfoFactory $paymentInfoFactory
     * @param PaymentInfoRepository $paymentInfoRepository
     */
    public function __construct(
        Config $config,
        ClientFactory $clientFactory,
        Xml $xmlConverter,
        ArrayManager $arrayManager,
        UrlInterface $urlBuilder,
        Session $customerSession,
        Cart $cart,
        Logger $logger,
        PaymentInfoFactory $paymentInfoFactory,
        PaymentInfoRepository $paymentInfoRepository
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->customerSession = $customerSession;
        $this->cart = $cart;
        parent::__construct(
            $config,
            $clientFactory,
            $xmlConverter,
            $arrayManager,
            $logger
        );
        $this->paymentInfoFactory = $paymentInfoFactory;
        $this->paymentInfoRepository = $paymentInfoRepository;
    }

    /**
     * Create transaction j4/j5 with/out invoice
     * @return string
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute()
    {
        $url = '';
        $cartQuote = $this->cart->getQuote();
        $grandTotal = $this->getTotal();

        $this->logger->info('------Token start (iframe)------');
        $this->logger->info(__(
            '$grandTotal = %1, quoteId = %2, customerId = %3, item_count = %4',
            $grandTotal,
            $cartQuote->getId(),
            $cartQuote->getCustomerId(),
            $cartQuote->getItemsCount()
        ));

        // for debug
        //$grandTotal = 0;
        //$grandTotal = '0.00';
        if ($grandTotal == 0 || $cartQuote->getItemsCount() == 0) {
            $message = __("Order total is zero or you don't have any items in cart. Please reload page");
            $this->logger->error($message);
            throw new LocalizedException($message);
        }

        $lang = $this->config->getLang();
        $payments = $this->config->getPaymentsNum($grandTotal);

        $xml = [
            'request' => [
                'version' => '2000',
                'language' => $lang,
                'dateTime' => '',
                'command' => 'doDeal',
                'requestId' => '',
                'doDeal' => [
                    'terminalNumber' => $this->config->getTid(),
                    'cardNo' => 'CGMPI',
                    'total' => $grandTotal * 100,
                    'transactionType' => 'Debit',
                    'creditType' => $payments === 1 ? 'RegularCredit' : 'Payments',
                    'numberOfPayments' => $payments === 1 ? '' : $payments,
                    'currency' => $lang === 'HEB' ? 'ILS' : 'USD',
                    'transactionCode' => 'Phone',
                    'validation' => 'TxnSetup',
                    'mid' => $this->config->getMid(),
                    'uniqueid' => time() . rand(100, 1000),
                    'mpiValidation' => 'Token',
                    'keepCD' => '1',
                    'successUrl' => $this->urlBuilder->getUrl('creditguard_emv/transaction/ok'),
                    'paymentPageData' => [
                        'useCvv' => 1,
                        'useId' => $this->config->useID()
                    ]
                ]
            ]
        ];

        $this->addPpsJsonConfig($xml);

        list($result, $error) = $this->request($xml);

        if (!\is_null($error)) {
            $url = $this->urlBuilder->getUrl('creditguard_emv/transaction/err', ['_query' => ['msg' => '191']]);
        } else {
            try {
                $this->logger->info('------Token Response (iframe)------');
                $this->logger->info(json_encode($result));
                $this->persistPaymentInfo($result, (int) $cartQuote->getId());
            } catch (LocalizedException $e) {
                return $this->urlBuilder->getUrl(
                    'creditguard_emv/transaction/err', ['_query' => ['msg' => $e->getMessage()]]
                );
            }
            $mpiUrl = $this->arrayManager->get(
                $this->arrayManager->findPath('mpiHostedPageUrl', $result),
                $result
            );

            if ($mpiUrl) {
                $url = $mpiUrl;
            } else {
                $err = $this->arrayManager->get(
                    $this->arrayManager->findPath('result', $result),
                    $result
                );
                $url = $err ?
                    $this->urlBuilder->getUrl('creditguard_emv/transaction/err', ['_query' => ['msg' => $err]]) :
                    $this->urlBuilder->getUrl('creditguard_emv/transaction/err', ['_query' => ['msg' => '192']]);
            }
        }

        return $url;
    }

    /**
     * @param $email
     * @return string
     */
    public function getCustomerName($email)
    {
        $name = $email;
        $s = $this->customerSession->getCustomer()->getEmail();
        if (isset($s)) {
            $name = $s;
        }

        return $name;
    }

    /**
     * @param array $result
     * @param int $quoteId
     * @throws LocalizedException
     */
    private function persistPaymentInfo(array $result, int $quoteId): void
    {
        if (!empty($result['response']['doDeal'])) {
            $doDeal = $result['response']['doDeal'];
            $this->validateResponse($doDeal);
            try {
                $paymentInfoData = $this->paymentInfoRepository->getByQuoteId($quoteId, TransactionType::IFRAME_URL);
            } catch (NoSuchEntityException $e) {
                $paymentInfoData = $this->paymentInfoFactory->create()->getDataModel();
            }

            $paymentInfoData->setQuoteId($quoteId);
            $paymentInfoData->setRequestId($doDeal['uniqueid']);
            $paymentInfoData->setTransactionId($doDeal['token']);
            $paymentInfoData->setType(TransactionType::IFRAME_URL);

            try {
                $this->paymentInfoRepository->save($paymentInfoData);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e->getTrace());
                throw new LocalizedException(__('Can\'t save CreditGuard payment info.'));
            }
        } else {
            throw new LocalizedException(__('doDeal node is empty in the CreditGuard response.'));
        }
    }

    /**
     * @param array $doDealNode
     * @throws LocalizedException
     */
    private function validateResponse(array $doDealNode): void
    {
        foreach ($this->validateFields as $field) {
            if (empty($doDealNode[$field])) {
                throw new LocalizedException(__('Empty %1 field in the CreditGuard response.', $field));
            }
        }
    }

    public function getTotal()
    {
        $cartQuote = $this->cart->getQuote();
        return $this->config->useTotal() ? $cartQuote->getGrandTotal() : $cartQuote->getBaseGrandTotal();
    }
}
