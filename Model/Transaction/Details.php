<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Transaction;

class Details extends AbstractTransaction
{

    /**
     * Get details on transaction
     * @param $req_id
     * @param $tx_id
     * @return mixed|string
     */
    public function getDetails($req_id, $tx_id)
    {
        $this->logger->info('------Start Get Details-' . $tx_id . '------');

        /*Build Ashrait XML to post*/
        $xml = [
            'request' => [
//                'requestId' => $req_id,
                'requestId' => '',
                'version' => '2000',
                'language' => 'HEB',
                'dateTime' =>'',
                'command' => 'inquireTransactions',
                'inquireTransactions' => [
                    'terminalNumber' => $this->config->getTid(),
                    'queryName' => 'mpiTransaction',
                    'mid' => $this->config->getMid(),
                    'mpiTransactionId' => $tx_id
                ]
            ]
        ];

        list($result, $error) = $this->request($xml);

        if (!\is_null($error)) {
            return $error->getMessage();
        }

        $this->logger->info('details_respond-' . print_r($result, true));

        return $result;
    }

}
