<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Transaction;

use Fisha\CreditGuardEMV\Model\Config;
use Fisha\CreditGuardEMV\Model\Logger\Logger;
use Magento\Framework\Convert\Xml;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Sales\Model\OrderFactory;

class Recreate extends AbstractTransaction
{
    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @param Config $config
     * @param ClientFactory $clientFactory
     * @param Xml $xmlConverter
     * @param ArrayManager $arrayManager
     * @param OrderFactory $orderFactory
     * @param Logger $logger
     */
    public function __construct(
        Config $config,
        ClientFactory $clientFactory,
        Xml $xmlConverter,
        ArrayManager $arrayManager,
        OrderFactory $orderFactory,
        Logger $logger
    ) {
        $this->orderFactory = $orderFactory;
        parent::__construct(
            $config,
            $clientFactory,
            $xmlConverter,
            $arrayManager,
            $logger
        );
    }

    /**
     * Do transaction from token j4 -j5 or re transaction
     *
     * @param $orderId
     * @param $amount
     * @param $exp
     * @param $card_id
     * @param $payments
     * @param $firstPayment
     * @param $periodicalPayment
     * @param $tranId
     * @param $authNumber
     * @param $validation
     * @param $sessionID
     * @return mixed|string
     * @throws NoSuchEntityException
     */
    public function doRecreate(
        $orderId,
        $amount,
        $exp,
        $card_id,
        $payments,
        $firstPayment,
        $periodicalPayment,
        $tranId,
        $authNumber,
        $validation,
        $sessionID = null
    ) {
        $this->logger->info('------doRecreate ' . $validation . ' start------');
        $this->logger->info(__(
            '$grandTotal = %1, quoteId = %2',
            $amount,
            $orderId
        ));

        $amount = (float) $amount * 100;
        $lang = $this->config->getLang();

        $xml = [
            'request' => [
                'version'        => '2000',
                'dateTime'       => '',
                'command'        => 'doDeal',
                'requestid'      => '',
                'mayBeDuplicate' => '0',
                'doDeal'         => [
                    //'token'             => $tranId,
                    'terminalNumber'    => $this->config->getTid(),
                    'numberOfPayments'  => (int) $payments >= 1 ? $payments : '',
                    'firstPayment'      => (int) $payments >= 1 ? $firstPayment : '',
                    'periodicalPayment' => (int) $payments >= 1 ? $periodicalPayment : '',
                    'cardId'            => $card_id,
                    'cardExpiration'    => $exp,
                    'transactionType'   => 'Debit',
                    'creditType'        => (int) $payments >= 1 ? 'Payments' : 'RegularCredit',
                    'currency'          => $lang === 'HEB' ? 'ILS' : 'USD',
                    'transactionCode'   => 'Phone',
                    'authNumber'        => $authNumber,
                    'useCD'             => $sessionID ? 1 : '',
                    'sessionCD'         => $sessionID,
                    'total'             => $amount,
                    'validation'        => $validation,
//                    'user'              => $orderId,
                ]
            ]
        ];
        $this->addPpsJsonConfig($xml);
        /*<version>2000</version>
          <language>ENG</language>
          <dateTime />
          <requestid />
          <command>doDeal</command>
          <doDeal>
             <terminalNumber>{{terminal}}</terminalNumber>
             <cardExpiration>{{cardExpiration}}</cardExpiration>
             <cardId>{{cardId}}</cardId>
             <transactionType>Debit</transactionType>
             <creditType>RegularCredit</creditType>
             <currency>{{currency}}</currency>
             <transactionCode>Phone</transactionCode>
             <total>{{total}}</total>
             <validation>Verify</validation>
             <useCD>1</useCD>
             <sessionCD>{{sessionCD}}</sessionCD>
          </doDeal>*/

        if ($this->config->getInvoice() && $orderId) {
            $order = $this->orderFactory->create()
                ->load($orderId);
            $orderItems = $order->getAllItems();

            $pid         = [];
            $description = [];
            $qty         = [];
            $price       = [];

            $email    = $order->getCustomerEmail();
            $discount = 0;

            foreach ($orderItems as $product) {
                $description[] = $product->getName();
                $price[]       = (float) $product->getBasePrice() * 100;
                $qty[]         = $product->getQtyOrdered();
                $pid[]         = $product->getId();
            }

            $description[] = $order->getShippingDescription();
            $price[]       = (float) $order->getBaseShippingAmount() * 100;
            $qty[]         = 1;
            $pid[]         = 1;

            $xml['request']['doDeal']['invoice'] = [
                'invoiceItemDescription' => implode('|', $description),
                'mailTo'                 => $email,
                'invoiceDate'            => date('Y-m-d'),
                'invoiceItemQuantity'    => implode('|', $qty),
                'invoiceSubject'         => $this->config->getSubject(),
                'invoiceItemPrice'       => implode('|', $price),
                'ccDate'                 => date('Y-m-d'),
                'invoiceDiscount'        => (string) ((float) $discount * 100),
                'invoiceTaxRate'         => '',
                'invoiceSignature'       => '',
                'invoiceDiscountRate'    => '',
                'invoiceComments'        => $this->config->getComment(),
                'invoiceItemCode'        => implode('|', $pid),
                'companyInfo'            => $this->config->getInfo(),
                'invoiceCreationMethod'  => 'wait',
                'isItemPriceWithTax'     => '1',
                'sendMail'               => '1',
                'invoiceType'            => 'taxreceipt'
            ];
        }

        list($result, $error) = $this->request($xml);

        if (!\is_null($error)) {
            return $error;
        }

        $this->logger->info('payment_respond-' . print_r($result, true));

        return $result;
    }

}
