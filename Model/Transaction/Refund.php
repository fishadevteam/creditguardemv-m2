<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Transaction;

use Magento\Payment\Model\InfoInterface;

class Refund extends AbstractTransaction
{
    /**
     * @param $payment
     * @param $amount
     * @return mixed
     */
    public function refund(InfoInterface $payment, $amount)
    {
        $xml = [
            'request' => [
                'command' => 'refundDeal',
                'requesteId' => '123', //ToDo: what is the requestID?
                'dateTime' => '',
                'version' => '2000',
                'language' => $this->config->getLang(),
                'refundDeal' => [
                    'terminalNumber' => $this->config->getTid(),
                    'tranId' => (int)$payment->getTransactionId(),
                    'total' => $amount * 100
                ]
            ]
        ];

        list($result, $error) = $this->request($xml);
        if (!\is_null($error)) {
            return $error;
        }

        $this->logger->info('refund_response - ' . print_r($result, true));
        return $result;
    }

}
