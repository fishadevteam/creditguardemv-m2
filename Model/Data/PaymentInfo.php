<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model\Data;

use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface;

class PaymentInfo extends \Magento\Framework\Api\AbstractExtensibleObject implements PaymentInfoInterface
{

    /**
     * Get entity_id
     * @return string|null
     */
    public function getEntityId()
    {
        return $this->_get(self::ENTITY_ID);
    }

    /**
     * Set entity_id
     * @param string $entityId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get quote_id
     * @return string|null
     */
    public function getQuoteId()
    {
        return $this->_get(self::QUOTE_ID);
    }

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setQuoteId($quoteId)
    {
        return $this->setData(self::QUOTE_ID, $quoteId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Fisha\CreditGuardEMV\Api\Data\PaymentInfoExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId()
    {
        return $this->_get(self::REQUEST_ID);
    }

    /**
     * Set request_id
     * @param string $requestId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setRequestId($requestId)
    {
        return $this->setData(self::REQUEST_ID, $requestId);
    }

    /**
     * Get transaction_id
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->_get(self::TRANSACTION_ID);
    }

    /**
     * Set transaction_id
     * @param string $transactionId
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setTransactionId($transactionId)
    {
        return $this->setData(self::TRANSACTION_ID, $transactionId);
    }

    /**
     * Get card_mask
     * @return string|null
     */
    public function getCardMask()
    {
        return $this->_get(self::CARD_MASK);
    }

    /**
     * Set card_mask
     * @param string $cardMask
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setCardMask($cardMask)
    {
        return $this->setData(self::CARD_MASK, $cardMask);
    }

    /**
     * Get card_mask
     * @return int
     */
    public function getType(): int
    {
        return (int) $this->_get(self::TYPE);
    }

    /**
     * Set card_mask
     * @param int $type
     * @return \Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface
     */
    public function setType(int $type)
    {
        return $this->setData(self::TYPE, $type);
    }

}

