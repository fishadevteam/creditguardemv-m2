<?php
/**
 *
 * @package    Fisha
 * @category   Getaway CreditGuard EMV
 * @author     Fisha Core Team
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Model\Logger;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Monolog\DateTimeImmutable;

class Logger extends \Monolog\Logger
{
    const XML_PATH_CREDITGUARD_DEBUG = 'payment/creditguard_emv/debug';

    protected $scopeConfig;

    /**
     * Logger constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param string $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        $name,
        $handlers = [],
        $processors = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $name,
            $handlers,
            $processors
        );
    }

    /**
     * {@inheritDoc}
     */
    public function addRecord(int $level, string $message, array $context = [], DateTimeImmutable $datetime = null): bool
    {
        if (!$this->configVal(self::XML_PATH_CREDITGUARD_DEBUG)) {
            return false;
        }

        return parent::addRecord($level, $message, $context, $datetime);
    }

    /**
     * @param $val
     * @return mixed
     */
    public function configVal($val)
    {
        return $this->scopeConfig->getValue($val, ScopeInterface::SCOPE_STORE);
    }
}
