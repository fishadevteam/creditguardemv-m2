<?php
declare(strict_types=1);

namespace Fisha\CreditGuardEMV\Model;

use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterface;
use Fisha\CreditGuardEMV\Api\Data\PaymentInfoInterfaceFactory;
use Fisha\CreditGuardEMV\Api\Data\PaymentInfoSearchResultsInterfaceFactory;
use Fisha\CreditGuardEMV\Api\PaymentInfoRepositoryInterface;
use Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo as ResourcePaymentInfo;
use Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo\Collection;
use Fisha\CreditGuardEMV\Model\ResourceModel\PaymentInfo\CollectionFactory as PaymentInfoCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PaymentInfoRepository implements PaymentInfoRepositoryInterface
{
    /**
     * @var PaymentInfoCollectionFactory
     */
    protected $paymentInfoCollectionFactory;

    /**
     * @var ResourcePaymentInfo
     */
    protected $resource;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var PaymentInfoSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var PaymentInfoFactory
     */
    protected $paymentInfoFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var PaymentInfoInterfaceFactory
     */
    protected $dataPaymentInfoFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param ResourcePaymentInfo $resource
     * @param PaymentInfoFactory $paymentInfoFactory
     * @param PaymentInfoInterfaceFactory $dataPaymentInfoFactory
     * @param PaymentInfoCollectionFactory $paymentInfoCollectionFactory
     * @param PaymentInfoSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePaymentInfo $resource,
        PaymentInfoFactory $paymentInfoFactory,
        PaymentInfoInterfaceFactory $dataPaymentInfoFactory,
        PaymentInfoCollectionFactory $paymentInfoCollectionFactory,
        PaymentInfoSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->paymentInfoFactory = $paymentInfoFactory;
        $this->paymentInfoCollectionFactory = $paymentInfoCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPaymentInfoFactory = $dataPaymentInfoFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        PaymentInfoInterface $paymentInfo
    ) {
        /* if (empty($paymentInfo->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $paymentInfo->setStoreId($storeId);
        } */

        $paymentInfoData = $this->extensibleDataObjectConverter->toNestedArray(
            $paymentInfo,
            [],
            PaymentInfoInterface::class
        );

        $paymentInfoModel = $this->paymentInfoFactory->create()->setData($paymentInfoData);

        try {
            $this->resource->save($paymentInfoModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the paymentInfo: %1',
                $exception->getMessage()
            ));
        }
        return $paymentInfoModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($paymentInfoId)
    {
        $paymentInfo = $this->paymentInfoFactory->create();
        $this->resource->load($paymentInfo, $paymentInfoId);
        if (!$paymentInfo->getId()) {
            throw new NoSuchEntityException(__('PaymentInfo with id "%1" does not exist.', $paymentInfoId));
        }
        return $paymentInfo->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->paymentInfoCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            PaymentInfoInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        PaymentInfoInterface $paymentInfo
    ) {
        try {
            $paymentInfoModel = $this->paymentInfoFactory->create();
            $this->resource->load($paymentInfoModel, $paymentInfo->getEntityId());
            $this->resource->delete($paymentInfoModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PaymentInfo: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($paymentInfoId)
    {
        return $this->delete($this->get($paymentInfoId));
    }

    /**
     * @param string $transactionId
     * @return PaymentInfoInterface
     * @throws NoSuchEntityException
     */
    public function getByTransactionId(string $transactionId)
    {
        $paymentInfo = $this->paymentInfoFactory->create();
        $this->resource->load($paymentInfo, $transactionId, PaymentInfoInterface::TRANSACTION_ID);
        if (!$paymentInfo->getId()) {
            throw new NoSuchEntityException(__('PaymentInfo with transaction_id "%1" does not exist.', $transactionId));
        }
        return $paymentInfo->getDataModel();
    }

    /**
     * @param string $quoteId
     * @param int $transactionType
     * @return PaymentInfoInterface
     * @throws NoSuchEntityException
     */
    public function getByQuoteId(string $quoteId, int $transactionType): PaymentInfoInterface
    {
        /** @var Collection $collection */
        $collection = $this->paymentInfoCollectionFactory->create();
        $collection->addFieldToFilter(PaymentInfoInterface::QUOTE_ID, $quoteId)
            ->addFieldToFilter(PaymentInfoInterface::TYPE, $transactionType);

        $paymentInfo = $collection->getFirstItem();

        if (!$paymentInfo->getId()) {
            throw new NoSuchEntityException(__('PaymentInfo with quote_id "%1" does not exist.', $quoteId));
        }
        return $paymentInfo->getDataModel();
    }

}
