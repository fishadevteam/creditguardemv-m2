<?php
/**
 *
 * @package    Fisha
 * @category   Credit Guard
 * @author     Fisha Core Team <kobi.p@fisha.co.il>
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\CreditGuardEMV\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class NumberOfPayments extends AbstractFieldArray
{

    /**
     * {@inheritDoc}
     */
    protected function _prepareToRender()
    {
        $this->addColumn('from', [
            'label' => __('From')
        ]);

        $this->addColumn('to', [
            'label' => __('To')
        ]);

        $this->addColumn('payments', [
            'label' => __('Number of Payments'),
            'class' => 'validate-not-negative-number'
        ]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Row');
    }

}
